'use strict'
var _l = require('lodash')
var async = require('async')
var moment = require('moment')
var fs = require('fs')
var readline = require('readline')
var google = require('googleapis')
var googleAuth = require('google-auth-library')
var Schedule = require('node-schedule')

var request = require('request')
var Request = require('./delayed_request.js')(require('request'))
var delayed_request = new Request({
  debug: false, // Optional, output delay to console 
  delayMin: 1000,
  delayMax: 3000
})

var spreadsheetId = '1hGAT__PeV7Mk5EZ-lC2ZxK-GAbAQ2_RbWS2DbosrbL4'

// spike
var spike_key = 'b6kI8VgHv44Ed'
var fb_post_uri = 'https://api.newswhip.com/v1/fbPosts?key=' + spike_key
var articles_uri = 'https://api.newswhip.com/v1/articles?key=' + spike_key

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
// var TOKEN_DIR = '/data/taskforce/motherlode-cron/'
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
  process.env.USERPROFILE) + '/Dev/integrate-report/'
var TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json'

var updateSpreadsheet = function () {
  // Load client secrets from a local file.
  fs.readFile('client_secret.json', function processClientSecrets (err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err)
      return
    }
    // Authorize a client with the loaded credentials, then call the
    // Google Sheets API.
    authorize(JSON.parse(content), updateSheets)
  })

  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   *
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  function authorize (credentials, callback) {
    var clientSecret = credentials.installed.client_secret
    var clientId = credentials.installed.client_id
    var redirectUrl = credentials.installed.redirect_uris[0]
    var auth = new googleAuth()
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl)

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function (err, token) {
      if (err) {
        getNewToken(oauth2Client, callback)
      } else {
        oauth2Client.credentials = JSON.parse(token)
        callback(oauth2Client)
      }
    })
  }

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   *
   * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback to call with the authorized
   *     client.
   */
  function getNewToken (oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    })
    console.log('Authorize this app by visiting this url: ', authUrl)
    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })
    rl.question('Enter the code from that page here: ', function (code) {
      rl.close()
      oauth2Client.getToken(code, function (err, token) {
        if (err) {
          console.log('Error while trying to retrieve access token', err)
          return
        }
        oauth2Client.credentials = token
        storeToken(token)
        callback(oauth2Client)
      })
    })
  }

  /**
   * Store token to disk be used in later program executions.
   *
   * @param {Object} token The token to store to disk.
   */
  function storeToken (token) {
    try {
      fs.mkdirSync(TOKEN_DIR)
    } catch (err) {
      if (err.code != 'EEXIST') {
        throw err
      }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token))
    console.log('Token stored to ' + TOKEN_PATH)
  }

  function updateSheets (auth) {
    var sheets = google.sheets('v4')
    sheets.spreadsheets.values.get({
      auth: auth,
      spreadsheetId: spreadsheetId,
      range: 'targets!A2:G',
      majorDimension: 'ROWS'
    }, function (err, response) {
      if (err) {
        console.log('get - the API returned an error: ' + err)
        return
        process.exit()
      }
      // console.log(JSON.stringify(response.values, null, 2))
      var update_list = []
      _l.forEach(response.values, function (item) {
        if (item.indexOf('y') > -1) {
          async.waterfall([
            // parse to integer, if fail, set to 72 hours 
            function (callback) {
              try {
                item[5] = parseInt(item[5])
              } catch (error) {
                console.log('error: ', error)
                console.log('duration is not integer, set to default setting: 72 hours')
                item[5] = 72
              }
              callback(null, item)
            },
            // if duration < 0, set to 72 hours
            function (item, callback) {
              if (item[5] < 0) {
                console.log('duration < 0, set to default setting: 72 hours')
                item[5] = 72
              }
              callback(null, item)
            },
            function (item, callback) {
              if (item[2] == 'web' && item[3] != '') {
                if (item[3].indexOf('https://api.newswhip.com/') == -1) {
                  if (item[0].indexOf('all') > -1) {
                    item[3] = 'https://api.newswhip.com/v1/region/' + item[3] + '/' + item[5] + '?key=' + spike_key
                  } else {
                    if (item[3].indexOf('OR') == -1) {
                      item[3] = 'https://api.newswhip.com/v1/publisher/' + item[3] + '/' + item[5] + '?key=' + spike_key
                    }
                  }
                }
              }
              callback(null, item)
            }
          ],
            // push to JSON
            function (err, item) {
              if (err) {
                console.log(JSON.stringify(err, null, 2))
              }
              if (item[2] == 'web' && item[3] != '') {
                if (item[3].indexOf('OR') > -1) {
                  update_list.push({
                    'name': item[0],
                    'type': item[2],
                    'publisher': '(' + item[3] + ')',
                    'duration': item[5],
                    'sort_by': item[6]
                  })
                } else {
                  update_list.push({
                    'name': item[0],
                    'type': item[2],
                    'url': item[3]
                  })
                }
              }
              if (item[2] == 'fb' && item[4] != '') {
                update_list.push({
                  'name': item[0],
                  'type': item[2],
                  'page_name': item[4],
                  'duration': item[5],
                  'sort_by': item[6]
                })
              }
            }
          )
        }
      }
      )
      // console.log(JSON.stringify(update_list, null, 2))

      async.eachSeries(update_list, function (update_sheet, next) {
        // console.log('update_sheet: ')
        // console.log(JSON.stringify(update_sheet, null, 2))
        setTimeout(function () {
          if (update_sheet.type == 'web') {
            if (update_sheet.hasOwnProperty('url')) {
              // crawler to get json
              delayed_request.run({
                url: update_sheet.url
              }, function (err, response, body) {
                if (err) {
                  console.log('err: ', err)
                  return
                }
                if (response.statusCode >= 400) {
                  console.log(update_sheet.name, ' response.statusCode: ', response.statusCode)
                  return
                }
                body = JSON.parse(body)
                // console.log(JSON.stringify(body, null, 2))
                // if get data clear sheets
                if (body.articles && body.articles.length > 0) {
                  sheets.spreadsheets.values.batchClear({
                    auth: auth,
                    spreadsheetId: spreadsheetId,
                    ranges: update_sheet.name + '!A1:G'
                  }, (err, response) => {
                    if (err) {
                      console.log(update_sheet.name, ' batchClear - the API returned an error: ' + err)
                      return
                    }
                    // console.log(JSON.stringify(response, null, 2))
                    if (response.clearedRanges) {
                      console.log(update_sheet.name + ' is cleared')

                      // Array will be insert
                      var updateArray = [
                        ['headline', 'publication_time', 'fb.total_engagement_count', 'publisher', 'thumbnail', 'link', 'last updated:' + moment().utcOffset('+0800').format('YYYY-MM-DD HH:mm:ss')]
                      ]
                      // arrange format
                      _l.forEach(body.articles, function (item) {
                        updateArray.push([item.headline, moment.unix(parseInt(item.publication_timestamp) / 1000).utcOffset('+0800').format('YYYY-MM-DD HH:mm'), item.fb_data.total_engagement_count, item.source.publisher, '=IMAGE("' + item.image_link + '", 1)', item.link])
                      })
                      // console.log(JSON.stringify(updateArray, null, 2))
                      // update sheets
                      console.log(update_sheet.name + ' is updating')
                      sheets.spreadsheets.values.update({
                        auth: auth,
                        spreadsheetId: spreadsheetId,
                        range: update_sheet.name + '!A1:G',
                        valueInputOption: 'USER_ENTERED',
                        resource: {
                          'values': updateArray
                        }
                      }, (err, response) => {
                        if (err) {
                          console.log(update_sheet.name, ' - update - the API returned an error: ' + err)
                          return
                        }
                        if (response) {
                          console.log(update_sheet.name + ' is updated')
                        }
                      })
                    }
                  })
                }
              })
            }
            if (update_sheet.hasOwnProperty('publisher')) {
              var sort_by = 'default'
              async.waterfall([
                function (callback) {
                  if (update_sheet.sort_by != undefined && update_sheet.sort_by != '') {
                    sort_by = update_sheet.sort_by
                    console.log(update_sheet.name + ' sort_by: ' + sort_by)
                  } else {
                    console.log(update_sheet.name + ' sort_by set to default')
                  }
                  callback(null, sort_by)
                }
              ], function (err, sort_by) {
                request.post({
                  headers: {
                    'content-type': 'application/json'
                  },
                  uri: articles_uri,
                  body: JSON.stringify({
                    'filters': ['publisher:' + update_sheet.publisher],
                    'from': (moment().utcOffset('+0800').unix() - 60 * 60 * update_sheet.duration) * 1000,
                    'sort_by': sort_by
                  })
                }, function (err, response, body) {
                  if (err) {
                    console.log('err: ', err)
                    return
                  }
                  if (response.statusCode >= 400) {
                    console.log(update_sheet.name, ' response.statusCode: ', response.statusCode)
                    return
                  }
                  body = JSON.parse(body)
                  // console.log(JSON.stringify(body, null, 2))
                  // if get data clear sheets
                  if (body.articles && body.articles.length > 0) {
                    sheets.spreadsheets.values.batchClear({
                      auth: auth,
                      spreadsheetId: spreadsheetId,
                      ranges: update_sheet.name + '!A1:H'
                    }, (err, response) => {
                      if (err) {
                        console.log(update_sheet.name, ' batchClear - the API returned an error: ' + err)
                        return
                      }
                      // console.log(JSON.stringify(response, null, 2))
                      if (response.clearedRanges) {
                        console.log(update_sheet.name + ' is cleared')

                        // Array will be insert
                        var updateArray = [
                          ['headline', 'excerpt', 'publication_time', 'fb.total_engagement_count', 'publisher', 'thumbnail', 'link', 'last updated:' + moment().utcOffset('+0800').format('YYYY-MM-DD HH:mm:ss')]
                        ]
                        // arrange format
                        _l.forEach(body.articles, function (item) {
                          updateArray.push([item.headline, item.excerpt, moment.unix(parseInt(item.publication_timestamp) / 1000).utcOffset('+0800').format('YYYY-MM-DD HH:mm'), item.fb_data.total_engagement_count, item.source.publisher, '=IMAGE("' + item.image_link + '", 1)', item.link])
                        })
                        // console.log(JSON.stringify(updateArray, null, 2))
                        // update sheets
                        console.log(update_sheet.name + ' is updating')
                        sheets.spreadsheets.values.update({
                          auth: auth,
                          spreadsheetId: spreadsheetId,
                          range: update_sheet.name + '!A1:H',
                          valueInputOption: 'USER_ENTERED',
                          resource: {
                            'values': updateArray
                          }
                        }, (err, response) => {
                          if (err) {
                            console.log(update_sheet.name, ' - update - the API returned an error: ' + err)
                            return
                          }
                          if (response) {
                            console.log(update_sheet.name + ' is updated')
                          }
                        })
                      }
                    })
                  }
                })
              })
            }
          }
          if (update_sheet.type == 'fb') {
            var sort_by = 'default'
            async.waterfall([
              function (callback) {
                var filter = []
                if (update_sheet.page_name.indexOf('country_code') > -1) {
                  filter = [update_sheet.page_name]
                } else {
                  filter = ['page_name:' + update_sheet.page_name]
                }
                callback(null, filter)
              },
              function (filter, callback) {
                if (update_sheet.sort_by != undefined && update_sheet.sort_by != '') {
                  sort_by = update_sheet.sort_by
                  console.log(update_sheet.name + ' sort_by: ' + sort_by)
                } else {
                  console.log(update_sheet.name + 'sort_by set to default')
                }
                callback(null, filter, sort_by)
              }
            ], function (err, filter, sort_by) {
              // crawler to get json
              request.post({
                headers: {
                  'content-type': 'application/json'
                },
                uri: fb_post_uri,
                body: JSON.stringify({
                  'filters': filter,
                  'from': (moment().utcOffset('+0800').unix() - 60 * 60 * update_sheet.duration) * 1000,
                  'sort_by': sort_by
                })
              }, function (err, response, body) {
                if (err) {
                  console.log('err: ', err)
                  return
                }
                if (response.statusCode >= 400) {
                  console.log(update_sheet.name, ' response.statusCode: ', response.statusCode)
                  return
                }
                body = JSON.parse(body)
                // console.log(JSON.stringify(body, null, 2))
                // if get data clear sheets
                if (body.fbPosts && body.fbPosts.length > 0) {
                  sheets.spreadsheets.values.batchClear({
                    auth: auth,
                    spreadsheetId: spreadsheetId,
                    ranges: update_sheet.name + '!A1:H'
                  }, (err, response) => {
                    if (err) {
                      console.log(update_sheet.name, ' batchClear - the API returned an error: ' + err)
                      return
                    }
                    // console.log(JSON.stringify(response, null, 2))
                    if (response.clearedRanges) {
                      console.log(update_sheet.name + ' is cleared')

                      // Array will be insert
                      var updateArray = [
                        ['headline', 'excerpt', 'publication_time', 'fb.total_engagement_count', 'page_name', 'thumbnail', 'link', 'last updated:' + moment().utcOffset('+0800').format('YYYY-MM-DD HH:mm:ss')]
                      ]
                      // arrange format
                      _l.forEach(body.fbPosts, function (item) {
                        updateArray.push([item.headline, item.excerpt, moment.unix(parseInt(item.publication_timestamp) / 1000).utcOffset('+0800').format('YYYY-MM-DD HH:mm'), item.fb_data.total_engagement_count, item.page_name, '=IMAGE("' + item.image_link + '", 1)', item.link])
                      })
                      // console.log(JSON.stringify(updateArray, null, 2))
                      // update sheets
                      console.log(update_sheet.name + ' is updating')
                      sheets.spreadsheets.values.update({
                        auth: auth,
                        spreadsheetId: spreadsheetId,
                        range: update_sheet.name + '!A1:H',
                        valueInputOption: 'USER_ENTERED',
                        resource: {
                          'values': updateArray
                        }
                      }, (err, response) => {
                        if (err) {
                          console.log(update_sheet.name, ' - update - the API returned an error: ' + err)
                          return
                        }
                        if (response) {
                          console.log(update_sheet.name + ' is updated')
                        }
                      })
                    }
                  })
                }
              })
            })
          }
          next()
        }, 3000)
      }, function () {
        // console.log('Done')
      })
    })
  }
}
// execute once to get token
updateSpreadsheet()

// Schedule.scheduleJob('* * * * *', updateSpreadsheet) // update every 1 minute
